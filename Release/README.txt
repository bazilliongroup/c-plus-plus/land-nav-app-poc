This application is a proof of concept application to test soldiers on their 
skills in Land Navigation. The resolution for this application is 500 meters,
so the person can be on (example: 500090, 500095, 505090, 505095) and the dot 
represents a platoon, person, or squad. 
.
To test soldiers on their land navigation, Press the HIDE MGRS, or SHOW MGRS 
button and it will via toggle dot out the MGRS coordinate and the soldier must
figure out the coordinate. they tell you the coordinate, hit the ShOW MGRS button
to see what the grid coordinate is.  