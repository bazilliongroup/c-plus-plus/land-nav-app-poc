#include "LandNavigation.h"

#define HIDE_TEXT L"Hide MGRS"
#define SHOW_TEXT L"Show MGRS"
#define NO_MGRS -1

LandNavigation::LandNavigation(CWnd* pParent): CDialog(LandNavigation::IDD, pParent) 
{
	//Dimentions of Map
	// x1,y1,x2,y2 as (width = x2-x1), (height = y2-y1)
	m_x1 = 150;
	m_y1 = 35;
	m_x2 = 300;
	m_y2 = 285;

	m_MGRS_CurrentCoordinate = 495115;
	m_nextLocation = CURRENT;

	InitilaizeMap();
}

BOOL LandNavigation::OnInitDialog() 
{ 
        CDialog::OnInitDialog();

		InitDialogItems();
		SetUpFontsAndColor();

		m_pHIDE_TOGGLE->ShowWindow( SW_HIDE );
		m_pNW->ShowWindow( SW_HIDE );
		m_pNORTH->ShowWindow( SW_HIDE );
		m_pNE->ShowWindow( SW_HIDE );
		m_pWEST->ShowWindow( SW_HIDE );
		m_pEAST->ShowWindow( SW_HIDE );
		m_pSW->ShowWindow( SW_HIDE );
		m_pSOUTH->ShowWindow( SW_HIDE );
		m_pSE->ShowWindow( SW_HIDE );
		m_pMGRS_Coordinate->ShowWindow( SW_HIDE );
        return true; 
}

void LandNavigation::InitDialogItems()
{
		//Initilaize all Resource items in INITIALIZE.RC, Everything works via pointers.
		m_pSTART = (CButton *)GetDlgItem( CB_START );
		m_pHIDE_TOGGLE = (CButton *)GetDlgItem( CB_HIDE_SHOW );
		m_pNW = (CButton *)GetDlgItem( CB_NORTH_WEST );
		m_pNORTH = (CButton *)GetDlgItem( CB_NORTH );
		m_pNE = (CButton *)GetDlgItem( CB_NORTH_EAST );
		m_pSW = (CButton *)GetDlgItem( CB_SOUTH_WEST );
		m_pSOUTH = (CButton *)GetDlgItem( CB_SOUTH );
		m_pSE = (CButton *)GetDlgItem( CB_SOUTH_EAST );
		m_pWEST = (CButton *)GetDlgItem( CB_WEST );
		m_pEAST = (CButton *)GetDlgItem( CB_EAST );
		m_pMGRS_Coordinate = (CStatic *)GetDlgItem( EC_MGRS );
}

void LandNavigation::RepaintVectorMap()
{
	//Arguments are: Left, Top, Right, Bottom
	CRect CLEARBOX(m_x1, m_y1, m_x2, m_y2);
	GetClientRect( &CLEARBOX );
	CLEARBOX.MoveToX(m_x1);
	CLEARBOX.MoveToY(m_y1);
	InvalidateRect(CLEARBOX, 1);
	//RedrawWindow(CLEARBOX, NULL, NULL, RDW_INVALIDATE);	//Updates the specified rectangle or region in the given window's client area.
}


void LandNavigation::STARTBUTTON()
{
	m_pSTART->EnableWindow( false );		//Disables the Start Button
	m_pNW->ShowWindow( SW_SHOW );
	m_pNORTH->ShowWindow( SW_SHOW );
	m_pNE->ShowWindow( SW_SHOW );
	m_pWEST->ShowWindow( SW_SHOW );
	m_pEAST->ShowWindow( SW_SHOW );
	m_pSW->ShowWindow( SW_SHOW );
	m_pSOUTH->ShowWindow( SW_SHOW );
	m_pSE->ShowWindow( SW_SHOW );
	m_pMGRS_Coordinate->ShowWindow( SW_SHOW );

	m_MGRS_Show = SHOW;
	m_pHIDE_TOGGLE->ShowWindow( SW_SHOW );
	m_pHIDE_TOGGLE->SetWindowTextW( HIDE_TEXT );
}

void LandNavigation::DIR_NORTHWEST()
{
	m_nextLocation = NORTHWEST;
	RepaintVectorMap();
}

void LandNavigation::DIR_NORTHEAST()
{
	m_nextLocation = NORTHEAST;
	RepaintVectorMap();
}

void LandNavigation::DIR_NORTH()
{
	m_nextLocation = NORTH;
	RepaintVectorMap();
}

void LandNavigation::DIR_WEST()
{
	m_nextLocation = WEST;
	RepaintVectorMap();
}

void LandNavigation::DIR_EAST()
{
	m_nextLocation = EAST;
	RepaintVectorMap();
}

void LandNavigation::DIR_SOUTHWEST()
{
	m_nextLocation = SOUTHWEST;
	RepaintVectorMap();
}

void LandNavigation::DIR_SOUTH()
{
	m_nextLocation = SOUTH;
	RepaintVectorMap();
}

void LandNavigation::DIR_SOUTHEAST()
{
	m_nextLocation = SOUTHEAST;
	RepaintVectorMap();
}

void LandNavigation::HIDESHOW()
{
	CString mgrsText;

	switch(m_MGRS_Show)
	{
		case SHOW:
			mgrsText.Format(L"-------");
			m_pHIDE_TOGGLE->SetWindowTextW( SHOW_TEXT );
			m_MGRS_Show = HIDE;
			break;
		case HIDE:
			mgrsText.Format(L"%d", m_MGRS_CurrentCoordinate);
			m_pHIDE_TOGGLE->SetWindowTextW( HIDE_TEXT );
			m_MGRS_Show = SHOW;
			break;
	}

	m_pMGRS_Coordinate->SetWindowTextW(mgrsText);

}

//Sets color in dialog
//pDC - pointer to device context
//pWnd - pointer to control asking for color information
//nCtlColor - symbolic constant for specifying MFC component
HBRUSH LandNavigation::OnCtlColor( CDC* pDC, CWnd* pWnd, UINT nCtlColor )
{
	switch (nCtlColor)
	{
		case CTLCOLOR_BTN:
		case CTLCOLOR_LISTBOX:
		case CTLCOLOR_MSGBOX:
		case CTLCOLOR_SCROLLBAR:
		case CTLCOLOR_EDIT:
		case CTLCOLOR_STATIC:
			pDC->SetTextColor(RGB(200,255,0));
			pDC->SetBkColor(RGB(0,0,0));		//Sets the text background color. Like a highlighter.
		case CTLCOLOR_DLG: 
			return m_Brush_Main;
		default: 
			return OnCtlColor(pDC, pWnd, nCtlColor); 
	}
}

void LandNavigation::SetUpFontsAndColor()
{
	m_Brush_Main.CreateSolidBrush(RGB(0,0,0));
}

void LandNavigation::OnPaint()
{
	CPaintDC dc( this );	//Create a Paint Object and put the current class as its object 
	DrawMap(dc);			//Draw using the Paint Object as its argument.
}

void LandNavigation::DrawMap( CPaintDC & dc )
{
	//Create Drawing Objects
	CPen MyPen;
	CBrush MyBrush;

	//Selecting an object returns the old one we need to catch and return it to avoid memory leaks
	CPen * pOldPen;
	CBrush* pOldBrush;

	//A Green brush                                         
	MyBrush.CreateSolidBrush(RGB(25, 181, 7)); 
	pOldBrush = dc.SelectObject(&MyBrush);

	//A red pen, one pixel wide
	MyPen.CreatePen(PS_SOLID, 1, RGB(255,0,0));
	pOldPen = dc.SelectObject(&MyPen);

	int offset = 25;

	//Setup a rectangle that is red on the outside, filled in blue 
	dc.Rectangle(m_x1, m_y1, m_x2, m_y2);	// x1,y1,x2,y2 as (width = x2-x1), (height = y2-y1) 
		
	//Setup x & y coordinates
		
	//veritcal lines
	dc.MoveTo(m_x1+offset*1, m_y1); 
	dc.LineTo(m_x1+offset*1, m_y2);
	dc.MoveTo(m_x1+offset*2, m_y1); 
	dc.LineTo(m_x1+offset*2, m_y2);
	dc.MoveTo(m_x1+offset*3, m_y1); 
	dc.LineTo(m_x1+offset*3, m_y2);
	dc.MoveTo(m_x1+offset*4, m_y1); 
	dc.LineTo(m_x1+offset*4, m_y2);
	dc.MoveTo(m_x1+offset*5, m_y1); 
	dc.LineTo(m_x1+offset*5, m_y2);

	//Horizontal lines
	dc.MoveTo(m_x1, m_y1+offset*1);
	dc.LineTo(m_x2, m_y1+offset*1);
	dc.MoveTo(m_x1, m_y1+offset*2);
	dc.LineTo(m_x2, m_y1+offset*2);
	dc.MoveTo(m_x1, m_y1+offset*3);
	dc.LineTo(m_x2, m_y1+offset*3);
	dc.MoveTo(m_x1, m_y1+offset*4);
	dc.LineTo(m_x2, m_y1+offset*4);
	dc.MoveTo(m_x1, m_y1+offset*5);
	dc.LineTo(m_x2, m_y1+offset*5);
	dc.MoveTo(m_x1, m_y1+offset*6);
	dc.LineTo(m_x2, m_y1+offset*6);
	dc.MoveTo(m_x1, m_y1+offset*7);
	dc.LineTo(m_x2, m_y1+offset*7);
	dc.MoveTo(m_x1, m_y1+offset*8);
	dc.LineTo(m_x2, m_y1+offset*8);
	dc.MoveTo(m_x1, m_y1+offset*9);
	dc.LineTo(m_x2, m_y1+offset*9);

	//Draw Backgrouand and Lines.
	dc.SelectObject(pOldBrush);
	dc.SelectObject(pOldPen);

	//Draw MGRS Coordinates 
	//

	CRect rectMGRS(0, 0, 0, 0);
	GetClientRect( &rectMGRS );

	CString mapMGRS_TopRow =  "57";
	rectMGRS.MoveToX( m_x1 - 20 );
	rectMGRS.MoveToY( m_y1 + 40 );
	dc.DrawText( mapMGRS_TopRow, -1, &rectMGRS, DT_SINGLELINE );

	CString mapMGRS_MidRow =  "53";
	rectMGRS.MoveToX( m_x1 - 20 );
	rectMGRS.MoveToY( m_y1 + 140  );
	dc.DrawText( mapMGRS_MidRow, -1, &rectMGRS, DT_SINGLELINE );

	CString mapMGRS_BotRow =  "49";
	rectMGRS.MoveToX( m_x1 - 20 );
	rectMGRS.MoveToY( m_y1 + 240  );
	dc.DrawText( mapMGRS_BotRow, -1, &rectMGRS, DT_SINGLELINE );

	CString mapMGRS_LeftCol =  "12";
	rectMGRS.MoveToX( m_x1 + 18 );
	rectMGRS.MoveToY( m_y1 - 20 );
	dc.DrawText( mapMGRS_LeftCol, -1, &rectMGRS, DT_SINGLELINE );

	CString mapMGRS_MidCol =   "14";
	rectMGRS.MoveToX( m_x1 + 68 );
	rectMGRS.MoveToY( m_y1 - 20 );
	dc.DrawText( mapMGRS_MidCol, -1, &rectMGRS, DT_SINGLELINE );

	CString mapMGRS_RightCol = "16";
	rectMGRS.MoveToX( m_x1 + 117 );
	rectMGRS.MoveToY( m_y1 - 20 );
	dc.DrawText( mapMGRS_RightCol, -1, &rectMGRS, DT_SINGLELINE );

	//Draw Player on MGRS Map
	PlatoonTracking( dc );
}

void LandNavigation::PlatoonTracking( CPaintDC & dc )
{
	//Create Drawing Objects
	CBrush* myBrush = new CBrush();

	myBrush->CreateSolidBrush( RGB(25, 0, 100) );
	dc.SelectObject( myBrush );
	CPen myPen( PS_SOLID, 2, RGB(0, 0, 0) );

	MGRS_PIXELS mgrsPixels;

	//This will return the Pixel Coodinates based on the MGRS coordinate and direction selected. 
	Find_MGRS( m_MGRS_CurrentCoordinate , m_nextLocation, mgrsPixels );

	CString mgrsText;
	if (m_MGRS_Show == SHOW)
	{
		mgrsText.Format(L"%d", m_MGRS_CurrentCoordinate);
		m_pMGRS_Coordinate->SetWindowTextW(mgrsText);
	}
	dc.Ellipse(mgrsPixels.m_upX, mgrsPixels.m_upY, mgrsPixels.m_lowX, mgrsPixels.m_lowY);
}

bool LandNavigation::Find_MGRS( int mgrs, NextLocation location, MGRS_PIXELS &pixelCoord )
{
	mgrs = GetNextCoordinate( mgrs, location );
	MGRS_COORDINATES_POSITIONS mgrsCoordPos = GetCoordinate( mgrs );
	pixelCoord = mgrsCoordPos.pixelCoord;
	m_MGRS_CurrentCoordinate = mgrs;

	return true;
}

void LandNavigation::SetCoordinate(int nw, int north, int ne, int west, int current, int east, int sw, int south, int se, int upX, int upY)
{
	MGRS_COORDINATES_POSITIONS coord;
	coord.northWest = nw;
	coord.north = north;
	coord.northEast = ne;
	coord.west = west;
	coord.current = current;
	coord.east = east;
	coord.southWest = sw;
	coord.south = south;
	coord.southEast = se;

	coord.pixelCoord.m_upX = upX;
	coord.pixelCoord.m_upY = upY;
	coord.pixelCoord.m_lowX = upX + 10;
	coord.pixelCoord.m_lowY = upY + 10;

	//Add the cord to CMap()
	m_PlayerPostions.SetAt( coord.current, coord );
}

LandNavigation::MGRS_COORDINATES_POSITIONS& LandNavigation::GetCoordinate( int mgrs )
{
	MGRS_COORDINATES_POSITIONS rValue;
	m_PlayerPostions.Lookup( mgrs, rValue );
	return rValue;
}

int LandNavigation::GetNextCoordinate( int mgrs, NextLocation location)
{
	int ret = NO_MGRS;

	MGRS_COORDINATES_POSITIONS rValue = GetCoordinate( mgrs );

	switch(location)
	{
		case NORTHWEST:
			ret = rValue.northWest;
			break;
		case NORTH:
			ret = rValue.north;
			break;
		case NORTHEAST:
			ret = rValue.northEast;
			break;
		case WEST:
			ret = rValue.west;
			break;
		case CURRENT:
			ret = rValue.current;
			break;
		case EAST:
			ret = rValue.east;
			break;
		case SOUTHWEST:
			ret = rValue.southWest;
			break;
		case SOUTH:
			ret = rValue.south;
			break;
		case SOUTHEAST:
			ret = rValue.southEast;
			break;
	}

	if (ret == NO_MGRS)
	{
		ret = mgrs; 
	}

	return ret;
}

void LandNavigation::InitilaizeMap()
{
	//                NW    NORTH      NE      WEST     CUR     EAST     SW      SOUTH      SE
	//----------------------------------------------------------------------------------------------------------
//	SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS, NO_MGRS, NO_MGRS, NO_MGRS, NO_MGRS, NO_MGRS, NO_MGRS , 0, 0 );

	//row 49 P2
	SetCoordinate( NO_MGRS, 505110,  500120,  NO_MGRS, 495115,  490125,	 NO_MGRS, NO_MGRS, NO_MGRS, ROW1, COL1 ); // 495115
	SetCoordinate( 505110,  500120,  505120,  495115,  490125,  495125,  NO_MGRS, NO_MGRS, NO_MGRS, ROW2, COL1 ); // 490125
	SetCoordinate( 500120,  505120,  500130,  490125,  495125,  490135,  NO_MGRS, NO_MGRS, NO_MGRS, ROW3, COL1 ); // 495125
	SetCoordinate( 505120,  500130,  505130,  495125,  490135,  495135,  NO_MGRS, NO_MGRS, NO_MGRS, ROW4, COL1 ); // 490135
	SetCoordinate( 500130,  505130,  500140,  490135,  495135,  490145,  NO_MGRS, NO_MGRS, NO_MGRS, ROW5, COL1 ); // 495135
	SetCoordinate( 505130,  500140,  505140,  495135,  490145,  495145,  NO_MGRS, NO_MGRS, NO_MGRS, ROW6, COL1 ); // 490145
	SetCoordinate( 500140,  505140,  500150,  490145,  495145,  490155,  NO_MGRS, NO_MGRS, NO_MGRS, ROW7, COL1 ); // 495145
	SetCoordinate( 505140,  500150,  505150,  495145,  490155,  495155,  NO_MGRS, NO_MGRS, NO_MGRS, ROW8, COL1 ); // 490155
	SetCoordinate( 500150,  505150,  500160,  490155,  495155,  490165,  NO_MGRS, NO_MGRS, NO_MGRS, ROW9, COL1 ); // 495155
	SetCoordinate( 505150,  500160,  505160,  495155,  490165,  495165,  NO_MGRS, NO_MGRS, NO_MGRS, ROW10, COL1 ); // 490165
	SetCoordinate( 500160,  505160,  NO_MGRS, 490165,  495165,  NO_MGRS, NO_MGRS, NO_MGRS, NO_MGRS, ROW11, COL1 ); // 495165

	//row 50 P1
	SetCoordinate( NO_MGRS, 505115,  500125,  NO_MGRS, 505110,  500120,	 NO_MGRS, 495115,  490125,  ROW1, COL2 ); // 505110
	SetCoordinate( 505115,  500125,  505125,  505110,  500120,  505120,  495115,  490125,  495125,  ROW2, 255 ); // 500120
	SetCoordinate( 500125,  505125,  500135,  500120,  505120,  500130,  490125,  495125,  490135,  ROW3, 255 ); // 505120
	SetCoordinate( 505125,  500135,  505135,  505120,  500130,  505130,  495125,  490135,  495135,  ROW4, 255 ); // 500130
	SetCoordinate( 500135,  505135,  500145,  500130,  505130,  500140,  490135,  495135,  490145,  ROW5, 255 ); // 505130
	SetCoordinate( 505135,  500145,  505145,  505130,  500140,  505140,  495135,  490145,  495145,  ROW6, 255 ); // 500140
	SetCoordinate( 500145,  505145,  500155,  500140,  505140,  500150,  490145,  495145,  490155,  ROW7, 255 ); // 505140
	SetCoordinate( 505145,  500155,  505155,  505140,  500150,  505150,  495145,  490155,  495155,  ROW8, 255 ); // 500150
	SetCoordinate( 500155,  505155,  500165,  500150,  505150,  500160,  490155,  495155,  490165,  ROW9, 255 ); // 505150
	SetCoordinate( 505155,  500165,  505165,  505150,  500160,  505160,  495155,  490165,  495165,  ROW10, 255 ); // 500160
	SetCoordinate( 500165,  505165,  NO_MGRS, 500160,  505160,  NO_MGRS, 490165,  495165,  NO_MGRS, ROW11, 255 ); // 505160

	//row 50 P2
	SetCoordinate( NO_MGRS, 515110,  510120,  NO_MGRS, 505115,  500125,	 NO_MGRS, 505110,  500120,  ROW1, COL3 ); // 505115
	SetCoordinate( 515110,  510120,  515120,  505115,  500125,  505125,  505110,  500120,  505120,  ROW2, 243 ); // 500125
	SetCoordinate( 510120,  515120,  510130,  500125,  505125,  500135,  500120,  505120,  500130,  ROW3, 243 ); // 505125
	SetCoordinate( 515120,  510130,  515130,  505125,  500135,  505135,  505120,  500130,  505130,  ROW4, 243 ); // 500135
	SetCoordinate( 510130,  515130,  510140,  500135,  505135,  500145,  500130,  505130,  500140,  ROW5, 243 ); // 505135
	SetCoordinate( 515130,  510140,  515140,  505135,  500145,  505145,  505130,  500140,  505140,  ROW6, 243 ); // 500145
	SetCoordinate( 510140,  515140,  510150,  500145,  505145,  500155,  500140,  505140,  500150,  ROW7, 243 ); // 505145
	SetCoordinate( 515140,  510150,  515150,  505145,  500155,  505155,  505140,  500150,  505150,  ROW8, 243 ); // 500155
	SetCoordinate( 510150,  515150,  510160,  500155,  505155,  500165,  500150,  505150,  500160,  ROW9, 243 ); // 505155
	SetCoordinate( 515150,  510160,  515160,  505155,  500165,  505165,  505150,  500160,  505160,  ROW10, 243 ); // 500165
	SetCoordinate( 510160,  515160,  NO_MGRS, 500165,  505165,  NO_MGRS, 500160,  505160,  NO_MGRS, ROW11, 243 ); // 505165

	//row 51 P1
	SetCoordinate( NO_MGRS, 515115,  510125,  NO_MGRS, 515110,  510120,	 NO_MGRS, 505115,  500125,  ROW1, COL4 ); // 515110
	SetCoordinate( 515115,  510125,  515125,  515110,  510120,  515120,  505115,  500125,  505125,  ROW2, 231 ); // 510120
	SetCoordinate( 510125,  515125,  510135,  510120,  515120,  510130,  500125,  505125,  500135,  ROW3, 231 ); // 515120
	SetCoordinate( 515125,  510135,  515135,  515120,  510130,  515130,  505125,  500135,  505135,  ROW4, 231 ); // 510130
	SetCoordinate( 510135,  515135,  510145,  510130,  515130,  510140,  500135,  505135,  500145,  ROW5, 231 ); // 515130
	SetCoordinate( 515135,  510145,  515145,  515130,  510140,  515140,  505135,  500145,  505145,  ROW6, 231 ); // 510140
	SetCoordinate( 510145,  515145,  510155,  510140,  515140,  510150,  500145,  505145,  500155,  ROW7, 231 ); // 515140
	SetCoordinate( 515145,  510155,  515155,  515140,  510150,  515150,  505145,  500155,  505155,  ROW8, 231 ); // 510150
	SetCoordinate( 510155,  515155,  510165,  510150,  515150,  510160,  500155,  505155,  500165,  ROW9, 231 ); // 515150
	SetCoordinate( 515155,  510165,  515165,  515150,  510160,  515160,  505155,  500165,  505165,  ROW10, 231 ); // 510160
	SetCoordinate( 510165,  515165,  NO_MGRS, 510160,  515160,  NO_MGRS, 500165,  505165,  NO_MGRS, ROW11, 231 ); // 515160

	//row 51 P2
	SetCoordinate( NO_MGRS, 525110,  520120,  NO_MGRS, 515115,  510125,	 NO_MGRS, 515110,  510120,  ROW1, COL5 ); // 515115
	SetCoordinate( 525110,  520120,  525120,  515115,  510125,  515125,  515110,  510120,  515120,  ROW2, 218 ); // 510125
	SetCoordinate( 520120,  525120,  520130,  510125,  515125,  510135,  510120,  515120,  510130,  ROW3, 218 ); // 515125
	SetCoordinate( 525120,  520130,  525130,  515125,  510135,  515135,  515120,  510130,  515130,  ROW4, 218 ); // 510135
	SetCoordinate( 520130,  525130,  520140,  510135,  515135,  510145,  510130,  515130,  510140,  ROW5, 218 ); // 515135
	SetCoordinate( 525130,  520140,  525140,  515135,  510145,  515145,  515130,  510140,  515140,  ROW6, 218 ); // 510145
	SetCoordinate( 520140,  525140,  520150,  510145,  515145,  510155,  510140,  515140,  510150,  ROW7, 218 ); // 515145
	SetCoordinate( 525140,  520150,  525150,  515145,  510155,  515155,  515140,  510150,  515150,  ROW8, 218 ); // 510155
	SetCoordinate( 520150,  525150,  520160,  510155,  515155,  510165,  510150,  515150,  510160,  ROW9, 218 ); // 515155
	SetCoordinate( 525150,  520160,  525160,  515155,  510165,  515165,  515150,  510160,  515160,  ROW10, 218 ); // 510165
	SetCoordinate( 520160,  525160,  NO_MGRS, 510165,  515165,  NO_MGRS, 510160,  515160,  NO_MGRS, ROW11, 218 ); // 515165

	//row 52 P1
	SetCoordinate( NO_MGRS, 525115,  520125,  NO_MGRS, 525110,  520120,	 NO_MGRS, 515115,  510125,  ROW1, COL6 ); // 525110
	SetCoordinate( 525115,  520125,  525125,  525110,  520120,  525120,  515115,  510125,  515125,  ROW2, 206 ); // 520120
	SetCoordinate( 520125,  525125,  520135,  520120,  525120,  520130,  510125,  515125,  510135,  ROW3, 206 ); // 525120
	SetCoordinate( 525125,  520135,  525135,  525120,  520130,  525130,  515125,  510135,  515135,  ROW4, 206 ); // 520130
	SetCoordinate( 520135,  525135,  520145,  520130,  525130,  520140,  510135,  515135,  510145,  ROW5, 206 ); // 525130
	SetCoordinate( 525135,  520145,  525145,  525130,  520140,  525140,  515135,  510145,  515145,  ROW6, 206 ); // 520140
	SetCoordinate( 520145,  525145,  520155,  520140,  525140,  520150,  510145,  515145,  510155,  ROW7, 206 ); // 525140
	SetCoordinate( 525145,  520155,  525155,  525140,  520150,  525150,  515145,  510155,  515155,  ROW8, 206 ); // 520150
	SetCoordinate( 520155,  525155,  520165,  520150,  525150,  520160,  510155,  515155,  510165,  ROW9, 206 ); // 525150
	SetCoordinate( 525155,  520165,  525165,  525150,  520160,  525160,  515155,  510165,  515165,  ROW10, 206 ); // 520160
	SetCoordinate( 520165,  525165,  NO_MGRS, 520160,  525160,  NO_MGRS, 510165,  515165,  NO_MGRS, ROW11, 206 ); // 525160

	//row 52 P2
	SetCoordinate( NO_MGRS, 535110,  530120,  NO_MGRS, 525115,  520125,	 NO_MGRS, 525110,  520120,  ROW1, COL7 ); // 525115
	SetCoordinate( 535110,  530120,  535120,  525115,  520125,  525125,  525110,  520120,  525120,  ROW2, 194 ); // 520125
	SetCoordinate( 530120,  535120,  530130,  520125,  525125,  520135,  520120,  525120,  520130,  ROW3, 194 ); // 525125
	SetCoordinate( 535120,  530130,  535130,  525125,  520135,  525135,  525120,  520130,  525130,  ROW4, 194 ); // 520135
	SetCoordinate( 530130,  535130,  530140,  520135,  525135,  520145,  520130,  525130,  520140,  ROW5, 194 ); // 525135
	SetCoordinate( 535130,  530140,  535140,  525135,  520145,  525145,  525130,  520140,  525140,  ROW6, 194 ); // 520145
	SetCoordinate( 530140,  535140,  530150,  520145,  525145,  520155,  520140,  525140,  520150,  ROW7, 194 ); // 525145
	SetCoordinate( 535140,  530150,  535150,  525145,  520155,  525155,  525140,  520150,  525150,  ROW8, 194 ); // 520155
	SetCoordinate( 530150,  535150,  530160,  520155,  525155,  520165,  520150,  525150,  520160,  ROW9, 194 ); // 525155
	SetCoordinate( 535150,  530160,  535160,  525155,  520165,  525165,  525150,  520160,  525160,  ROW10, 194 ); // 520165
	SetCoordinate( 530160,  535160,  NO_MGRS, 520165,  525165,  NO_MGRS, 520160,  525160,  NO_MGRS, ROW11, 194 ); // 525165

	//row 53 P1
	SetCoordinate( NO_MGRS, 535115,  530125,  NO_MGRS, 535110,  530120,	 NO_MGRS, 525115,  520125,  ROW1, COL8 ); // 535110
	SetCoordinate( 535115,  530125,  535125,  535110,  530120,  535120,  525115,  520125,  525125,  ROW2, 181 ); // 530120
	SetCoordinate( 530125,  535125,  530135,  530120,  535120,  530130,  520125,  525125,  520135,  ROW3, 181 ); // 535120
	SetCoordinate( 535125,  530135,  535135,  535120,  530130,  535130,  525125,  520135,  525135,  ROW4, 181 ); // 530130
	SetCoordinate( 530135,  535135,  530145,  530130,  535130,  530140,  520135,  525135,  520145,  ROW5, 181 ); // 535130
	SetCoordinate( 535135,  530145,  535145,  535130,  530140,  535140,  525135,  520145,  525145,  ROW6, 181 ); // 530140
	SetCoordinate( 530145,  535145,  530155,  530140,  535140,  530150,  520145,  525145,  520155,  ROW7, 181 ); // 535140
	SetCoordinate( 535145,  530155,  535155,  535140,  530150,  535150,  525145,  520155,  525155,  ROW8, 181 ); // 530150
	SetCoordinate( 530155,  535155,  530165,  530150,  535150,  530160,  520155,  525155,  520165,  ROW9, 181 ); // 535150
	SetCoordinate( 535155,  530165,  535165,  535150,  530160,  535160,  525155,  520165,  525165,  ROW10, 181 ); // 530160
	SetCoordinate( 530165,  535165,  NO_MGRS, 530160,  535160,  NO_MGRS, 520165,  525165,  NO_MGRS, ROW11, 181 ); // 535160

	//row 53 P2
	SetCoordinate( NO_MGRS, 545110,  540120,  NO_MGRS, 535115,  530125,	 NO_MGRS, 535110,  530120,  ROW1, COL9 ); // 535115
	SetCoordinate( 545110,  540120,  545120,  535115,  530125,  535125,  535110,  530120,  535120,  ROW2, 168 ); // 530125
	SetCoordinate( 540120,  545120,  540130,  530125,  535125,  530135,  530120,  535120,  530130,  ROW3, 168 ); // 535125
	SetCoordinate( 545120,  540130,  545130,  535125,  530135,  535135,  535120,  530130,  535130,  ROW4, 168 ); // 530135
	SetCoordinate( 540130,  545130,  540140,  530135,  535135,  530145,  530130,  535130,  530140,  ROW5, 168 ); // 535135
	SetCoordinate( 545130,  540140,  545140,  535135,  530145,  535145,  535130,  530140,  535140,  ROW6, 168 ); // 530145
	SetCoordinate( 540140,  545140,  540150,  530145,  535145,  530155,  530140,  535140,  530150,  ROW7, 168 ); // 535145
	SetCoordinate( 545140,  540150,  545150,  535145,  530155,  535155,  535140,  530150,  535150,  ROW8, 168 ); // 530155
	SetCoordinate( 540150,  545150,  540160,  530155,  535155,  530165,  530150,  535150,  530160,  ROW9, 168 ); // 535155
	SetCoordinate( 545150,  540160,  545160,  535155,  530165,  535165,  535150,  530160,  535160,  ROW10, 168 ); // 530165
	SetCoordinate( 540160,  545160,  NO_MGRS, 530165,  535165,  NO_MGRS, 530160,  535160,  NO_MGRS, ROW11, 168 ); // 535165

	//row 54 P1	
	SetCoordinate( NO_MGRS, 545115,  540125,  NO_MGRS, 545110,  540120,	 NO_MGRS, 535115,  530125,  ROW1, COL10 ); // 5495110
	SetCoordinate( 545115,  540125,  545125,  545110,  540120,  545120,  535115,  530125,  535125,  ROW2, 156 ); // 535125
	SetCoordinate( 540125,  545125,  540135,  540120,  545120,  540130,  530125,  535125,  530135,  ROW3, 156 ); // 545120
	SetCoordinate( 545125,  540135,  545135,  545120,  540130,  545130,  535125,  530135,  535135,  ROW4, 156 ); // 540130
	SetCoordinate( 540135,  545135,  540145,  540130,  545130,  540140,  530135,  535135,  530145,  ROW5, 156 ); // 545130
	SetCoordinate( 545135,  540145,  545145,  545130,  540140,  545140,  535135,  530145,  535145,  ROW6, 156 ); // 540140
	SetCoordinate( 540145,  545145,  540155,  540140,  545140,  540150,  530145,  535145,  530155,  ROW7, 156 ); // 545140
	SetCoordinate( 545145,  540155,  545155,  545140,  540150,  545150,  535145,  530155,  535155,  ROW8, 156 ); // 540150
	SetCoordinate( 540155,  545155,  540165,  540150,  545150,  540160,  530155,  535155,  530165,  ROW9, 156 ); // 545150
	SetCoordinate( 545155,  540165,  545165,  545150,  540160,  545160,  535155,  530165,  535165,  ROW10, 156 ); // 540160
	SetCoordinate( 540165,  545165,  NO_MGRS, 540160,  545160,  NO_MGRS, 530165,  535165,  NO_MGRS, ROW11, 156 ); // 545160

	//row 54 P2	
	SetCoordinate( NO_MGRS, 555110,  550120,  NO_MGRS, 545115,  540125,	 NO_MGRS, 545110,  540120,  ROW1, COL11 ); // 545115
	SetCoordinate( 555110,  550120,  555120,  545115,  540125,  545125,  545110,  540120,  545120,  ROW2, 143 ); // 540125
	SetCoordinate( 550120,  555120,  550130,  540125,  545125,  540135,  540120,  545120,  540130,  ROW3, 143 ); // 545125
	SetCoordinate( 555120,  550130,  555130,  545125,  540135,  545135,  545120,  540130,  545130,  ROW4, 143 ); // 540135
	SetCoordinate( 550130,  555130,  550140,  540135,  545135,  540145,  540130,  545130,  540140,  ROW5, 143 ); // 545135
	SetCoordinate( 555130,  550140,  555140,  545135,  540145,  545145,  545130,  540140,  545140,  ROW6, 143 ); // 540145
	SetCoordinate( 550140,  555140,  550150,  540145,  545145,  540155,  540140,  545140,  540150,  ROW7, 143 ); // 545145
	SetCoordinate( 555140,  550150,  555150,  545145,  540155,  545155,  545140,  540150,  545150,  ROW8, 143 ); // 540155
	SetCoordinate( 550150,  555150,  550160,  540155,  545155,  540165,  540150,  545150,  540160,  ROW9, 143 ); // 545155
	SetCoordinate( 555150,  550160,  555160,  545155,  540165,  545165,  545150,  540160,  545160,  ROW10, 143 ); // 540165
	SetCoordinate( 550160,  555160,  NO_MGRS, 540165,  545165,  NO_MGRS, 540160,  545160,  NO_MGRS, ROW11, 143 ); // 545165

	//row 55 P1	
	SetCoordinate( NO_MGRS, 555115,  550125,  NO_MGRS, 555110,  550120,	 NO_MGRS, 545115,  540125,  ROW1, COL12 ); // 555110
	SetCoordinate( 555115,  550125,  555125,  555110,  550120,  555120,  545115,  540125,  545125,  ROW2, 130 ); // 555125
	SetCoordinate( 550125,  555125,  550135,  550120,  555120,  550130,  540125,  545125,  540135,  ROW3, 130 ); // 555120
	SetCoordinate( 555125,  550135,  555135,  555120,  550130,  555130,  545125,  540135,  545135,  ROW4, 130 ); // 550130
	SetCoordinate( 550135,  555135,  550145,  550130,  555130,  550140,  540135,  545135,  540145,  ROW5, 130 ); // 555130
	SetCoordinate( 555135,  550145,  555145,  555130,  550140,  555140,  545135,  540145,  545145,  ROW6, 130 ); // 550140
	SetCoordinate( 550145,  555145,  550155,  550140,  555140,  550150,  540145,  545145,  540155,  ROW7, 130 ); // 555140
	SetCoordinate( 555145,  550155,  555155,  555140,  550150,  555150,  545145,  540155,  545155,  ROW8, 130 ); // 550150
	SetCoordinate( 550155,  555155,  550165,  550150,  555150,  550160,  540155,  545155,  540165,  ROW9, 130 ); // 555150
	SetCoordinate( 555155,  550165,  555165,  555150,  550160,  555160,  545155,  540165,  545165,  ROW10, 130 ); // 550160
	SetCoordinate( 550165,  555165,  NO_MGRS, 550160,  555160,  NO_MGRS, 540165,  545165,  NO_MGRS, ROW11, 130 ); // 555160

	//row 55 P2	
	SetCoordinate( NO_MGRS, 565110,  560120,  NO_MGRS, 555115,  550125,	 NO_MGRS, 555110,  550120,  ROW1, COL13 ); // 555115
	SetCoordinate( 565110,  560120,  565120,  555115,  550125,  555125,  555110,  550120,  555120,  ROW2, 118 ); // 550125
	SetCoordinate( 560120,  565120,  560130,  550125,  555125,  550135,  550120,  555120,  550130,  ROW3, 118 ); // 555125
	SetCoordinate( 565120,  560130,  565130,  555125,  550135,  555135,  555120,  550130,  555130,  ROW4, 118 ); // 550135
	SetCoordinate( 560130,  565130,  560140,  550135,  555135,  550145,  550130,  555130,  550140,  ROW5, 118 ); // 555135
	SetCoordinate( 565130,  560140,  565140,  555135,  550145,  555145,  555130,  550140,  555140,  ROW6, 118 ); // 550145
	SetCoordinate( 560140,  565140,  560150,  550145,  555145,  550155,  550140,  555140,  550150,  ROW7, 118 ); // 555145
	SetCoordinate( 565140,  560150,  565150,  555145,  550155,  555155,  555140,  550150,  555150,  ROW8, 118 ); // 550155
	SetCoordinate( 560150,  565150,  560160,  550155,  555155,  550165,  550150,  555150,  550160,  ROW9, 118 ); // 555155
	SetCoordinate( 565150,  560160,  565160,  555155,  550165,  555165,  555150,  550160,  555160,  ROW10, 118 ); // 550165
	SetCoordinate( 560160,  565160,  NO_MGRS, 550165,  555165,  NO_MGRS, 550160,  555160,  NO_MGRS, ROW11, 118 ); // 555165

	//row 56 P1	
	SetCoordinate( NO_MGRS, 565115,  560125,  NO_MGRS, 565110,  560120,	 NO_MGRS, 555115,  550125,  ROW1, COL14 ); // 565110
	SetCoordinate( 565115,  560125,  565125,  565110,  560120,  565120,  555115,  550125,  555125,  ROW2, 106 ); // 565125
	SetCoordinate( 560125,  565125,  560135,  560120,  565120,  560130,  550125,  555125,  550135,  ROW3, 106 ); // 565120
	SetCoordinate( 565125,  560135,  565135,  565120,  560130,  565130,  555125,  550135,  555135,  ROW4, 106 ); // 560130
	SetCoordinate( 560135,  565135,  560145,  560130,  565130,  560140,  550135,  555135,  550145,  ROW5, 106 ); // 565130
	SetCoordinate( 565135,  560145,  565145,  565130,  560140,  565140,  555135,  550145,  555145,  ROW6, 106 ); // 560140
	SetCoordinate( 560145,  565145,  560155,  560140,  565140,  560150,  550145,  555145,  550155,  ROW7, 106 ); // 565140
	SetCoordinate( 565145,  560155,  565155,  565140,  560150,  565150,  555145,  550155,  555155,  ROW8, 106 ); // 560150
	SetCoordinate( 560155,  565155,  560165,  560150,  565150,  560160,  550155,  555155,  550165,  ROW9, 106 ); // 565150
	SetCoordinate( 565155,  560165,  565165,  565150,  560160,  565160,  555155,  550165,  555165,  ROW10, 106 ); // 560160
	SetCoordinate( 560165,  565165,  NO_MGRS, 560160,  565160,  NO_MGRS, 550165,  555165,  NO_MGRS, ROW11, 106 ); // 565160

	//row 56 P2	
	SetCoordinate( NO_MGRS, 575110,  570120,  NO_MGRS, 565115,  560125,	 NO_MGRS, 565110,  560120,  ROW1, COL15 ); // 565115
	SetCoordinate( 575110,  570120,  575120,  565115,  560125,  565125,  565110,  560120,  565120,  ROW2, 94 ); // 560125
	SetCoordinate( 570120,  575120,  570130,  560125,  565125,  560135,  560120,  565120,  560130,  ROW3, 94 ); // 565125
	SetCoordinate( 575120,  570130,  575130,  565125,  560135,  565135,  565120,  560130,  565130,  ROW4, 94 ); // 560135
	SetCoordinate( 570130,  575130,  570140,  560135,  565135,  560145,  560130,  565130,  560140,  ROW5, 94 ); // 565135
	SetCoordinate( 575130,  570140,  575140,  565135,  560145,  565145,  565130,  560140,  565140,  ROW6, 94 ); // 560145
	SetCoordinate( 570140,  575140,  570150,  560145,  565145,  560155,  560140,  565140,  560150,  ROW7, 94 ); // 565145
	SetCoordinate( 575140,  570150,  575150,  565145,  560155,  565155,  565140,  560150,  565150,  ROW8, 94 ); // 560155
	SetCoordinate( 570150,  575150,  570160,  560155,  565155,  560165,  560150,  565150,  560160,  ROW9, 94 ); // 565155
	SetCoordinate( 575150,  570160,  575160,  565155,  560165,  565165,  565150,  560160,  565160,  ROW10, 94 ); // 560165
	SetCoordinate( 570160,  575160,  NO_MGRS, 560165,  565165,  NO_MGRS, 560160,  565160,  NO_MGRS, ROW11, 94 ); // 565165

	//row 57 P1	
	SetCoordinate( NO_MGRS, 575115,  570125,  NO_MGRS, 575110,  570120,	 NO_MGRS, 565115,  560125,  ROW1, COL16 ); // 575110
	SetCoordinate( 575115,  570125,  575125,  575110,  570120,  575120,  565115,  560125,  565125,  ROW2, 81 ); // 575125
	SetCoordinate( 570125,  575125,  570135,  570120,  575120,  570130,  560125,  565125,  560135,  ROW3, 81 ); // 575120
	SetCoordinate( 575125,  570135,  575135,  575120,  570130,  575130,  565125,  560135,  565135,  ROW4, 81 ); // 570130
	SetCoordinate( 570135,  575135,  570145,  570130,  575130,  570140,  560135,  565135,  560145,  ROW5, 81 ); // 575130
	SetCoordinate( 575135,  570145,  575145,  575130,  570140,  575140,  565135,  560145,  565145,  ROW6, 81 ); // 570140
	SetCoordinate( 570145,  575145,  570155,  570140,  575140,  570150,  560145,  565145,  560155,  ROW7, 81 ); // 575140
	SetCoordinate( 575145,  570155,  575155,  575140,  570150,  575150,  565145,  560155,  565155,  ROW8, 81 ); // 570150
	SetCoordinate( 570155,  575155,  570165,  570150,  575150,  570160,  560155,  565155,  560165,  ROW9, 81 ); // 575150
	SetCoordinate( 575155,  570165,  575165,  575150,  570160,  575160,  565155,  560165,  565165,  ROW10, 81 ); // 570160
	SetCoordinate( 570165,  575165,  NO_MGRS, 570160,  575160,  NO_MGRS, 560165,  565165,  NO_MGRS, ROW11, 81 ); // 575160

	//row 57 P2	
	SetCoordinate( NO_MGRS, 585110,  580120,  NO_MGRS, 575115,  570125,	 NO_MGRS, 575110,  570120,  ROW1, COL17 ); // 575115
	SetCoordinate( 585110,  580120,  585120,  575115,  570125,  575125,  575110,  570120,  575120,  ROW2, 68 ); // 570125
	SetCoordinate( 580120,  585120,  580130,  570125,  575125,  570135,  570120,  575120,  570130,  ROW3, 68 ); // 575125
	SetCoordinate( 585120,  580130,  585130,  575125,  570135,  575135,  575120,  570130,  575130,  ROW4, 68 ); // 570135
	SetCoordinate( 580130,  585130,  580140,  570135,  575135,  570145,  570130,  575130,  570140,  ROW5, 68 ); // 575135
	SetCoordinate( 585130,  580140,  585140,  575135,  570145,  575145,  575130,  570140,  575140,  ROW6, 68 ); // 570145
	SetCoordinate( 580140,  585140,  580150,  570145,  575145,  570155,  570140,  575140,  570150,  ROW7, 68 ); // 575145
	SetCoordinate( 585140,  580150,  585150,  575145,  570155,  575155,  575140,  570150,  575150,  ROW8, 68 ); // 570155
	SetCoordinate( 580150,  585150,  580160,  570155,  575155,  570165,  570150,  575150,  570160,  ROW9, 68 ); // 575155
	SetCoordinate( 585150,  580160,  585160,  575155,  570165,  575165,  575150,  570160,  575160,  ROW10, 68 ); // 570165
	SetCoordinate( 580160,  585160,  NO_MGRS, 570165,  575165,  NO_MGRS, 570160,  575160,  NO_MGRS, ROW11, 68 ); // 575165

	//row 58 P1	
	SetCoordinate( NO_MGRS, 585115,  580125,  NO_MGRS, 585110,  580120,	 NO_MGRS, 575115,  570125,  ROW1, COL18 ); // 585110
	SetCoordinate( 585115,  580125,  585125,  585110,  580120,  585120,  575115,  570125,  575125,  ROW2, 56 ); // 585125
	SetCoordinate( 580125,  585125,  580135,  580120,  585120,  580130,  570125,  575125,  570135,  ROW3, 56 ); // 585120
	SetCoordinate( 585125,  580135,  585135,  585120,  580130,  585130,  575125,  570135,  575135,  ROW4, 56 ); // 580130
	SetCoordinate( 580135,  585135,  580145,  580130,  585130,  580140,  570135,  575135,  570145,  ROW5, 56 ); // 585130
	SetCoordinate( 585135,  580145,  585145,  585130,  580140,  585140,  575135,  570145,  575145,  ROW6, 56 ); // 580140
	SetCoordinate( 580145,  585145,  580155,  580140,  585140,  580150,  570145,  575145,  570155,  ROW7, 56 ); // 585140
	SetCoordinate( 585145,  580155,  585155,  585140,  580150,  585150,  575145,  570155,  575155,  ROW8, 56 ); // 580150
	SetCoordinate( 580155,  585155,  580165,  580150,  585150,  580160,  570155,  575155,  570165,  ROW9, 56 ); // 585150
	SetCoordinate( 585155,  580165,  585165,  585150,  580160,  585160,  575155,  570165,  575165,  ROW10, 56 ); // 580160
	SetCoordinate( 580165,  585165,  NO_MGRS, 580160,  585160,  NO_MGRS, 570165,  575165,  NO_MGRS, ROW11, 56 ); // 585160

	//row 58 P2	
	SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  NO_MGRS, 585115,  580125,  NO_MGRS, 585110,  580120,  ROW1, COL19 ); // 585115
	SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  585115,  580125,  585125,  585110,  580120,  585120,  ROW2, 43 ); // 580125
	SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  580125,  585125,  580135,  580120,  585120,  580130,  ROW3, 43 ); // 585125
	SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  585125,  580135,  585135,  585120,  580130,  585130,  ROW4, 43 ); // 580135
	SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  580135,  585135,  580145,  580130,  585130,  580140,  ROW5, 43 ); // 585135
	SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  585135,  580145,  585145,  585130,  580140,  585140,  ROW6, 43 ); // 580145
	SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  580145,  585145,  580155,  580140,  585140,  580150,  ROW7, 43 ); // 585145
	SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  585145,  580155,  585155,  585140,  580150,  585150,  ROW8, 43 ); // 580155
	SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  580155,  585155,  580165,  580150,  585150,  580160,  ROW9, 43 ); // 585155
	SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  585155,  580165,  585165,  585150,  580160,  585160,  ROW10, 43 ); // 580165
	SetCoordinate( NO_MGRS, NO_MGRS, NO_MGRS,  580165,  585165,  NO_MGRS, 580160,  585160,  NO_MGRS, ROW11, 43 ); // 585165
}