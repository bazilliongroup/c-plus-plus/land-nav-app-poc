#pragma once

#include "globals.h"
#include <afxwin.h>      //MFC core and standard components
#include "resource.h"    //main symbols


class LandNavigation : public CDialog
{

public:

    LandNavigation(CWnd* pParent = NULL);

    // Dialog Data, name of dialog form
    enum{IDD = INTERFACE1};

protected:
    virtual void DoDataExchange(CDataExchange* pDX) { CDialog::DoDataExchange(pDX); }
	virtual void OnPaint();

	HBRUSH OnCtlColor( CDC* pDC, CWnd* pWnd, UINT nCtlColor );

public:

//-----------------------------------------------------------------
	afx_msg void start() { STARTBUTTON(); }
	afx_msg void hideShow() { HIDESHOW(); }
	afx_msg void dirNorthWest() { DIR_NORTHWEST(); }
	afx_msg void dirNorthEast() { DIR_NORTHEAST(); }
	afx_msg void dirNorth() { DIR_NORTH(); }
	afx_msg void dirWest() { DIR_WEST(); }
	afx_msg void dirEast() { DIR_EAST(); }
	afx_msg void dirSouthWest() { DIR_SOUTHWEST(); }
	afx_msg void dirSouth() { DIR_SOUTH(); }
	afx_msg void dirSouthEast() { DIR_SOUTHEAST(); }
//-----------------------------------------------------------------

	void STARTBUTTON();
	void HIDESHOW();
	void DIR_NORTHWEST();
	void DIR_NORTHEAST();
	void DIR_NORTH();
	void DIR_WEST();
	void DIR_EAST();
	void DIR_SOUTHWEST();
	void DIR_SOUTH();
	void DIR_SOUTHEAST();

DECLARE_MESSAGE_MAP()

private:
	CButton* m_pSTART;
	CButton* m_pHIDE_TOGGLE;
	CButton* m_pNW;
	CButton* m_pNORTH;
	CButton* m_pNE;
	CButton* m_pSW;
	CButton* m_pSOUTH;
	CButton* m_pSE;
	CButton* m_pWEST;
	CButton* m_pEAST;
	CStatic* m_pMGRS_Coordinate;

	CBrush m_Brush_Main;
	int m_x1, m_y1, m_x2, m_y2;		//Coordinate Dimentions of Map
	int m_MGRS_CurrentCoordinate;

	enum NextLocation { EAST, WEST, NORTH, SOUTH, NORTHEAST, NORTHWEST, SOUTHEAST, SOUTHWEST, CURRENT };
	enum HideShow { HIDE, SHOW };
	
	NextLocation m_nextLocation;
	HideShow m_MGRS_Show;

	typedef struct  mgrsPixels
	{
		mgrsPixels():m_upX(0), m_upY(0), m_lowX(0), m_lowY(0) { }

		int m_upX, m_upY, m_lowX, m_lowY;

	}MGRS_PIXELS;

	typedef struct mgrsCoordinatePositions
	{
		mgrsCoordinatePositions(): current(0), north(0), south(0), east(0), west(0), northWest(0), northEast(0), southWest(0), southEast(0) {}

		int current;
		int north, south, east, west;
		int northWest, northEast;
		int southWest, southEast;

		MGRS_PIXELS pixelCoord;

	}MGRS_COORDINATES_POSITIONS;

	CMap<int, int, MGRS_COORDINATES_POSITIONS, MGRS_COORDINATES_POSITIONS> m_PlayerPostions;

	virtual BOOL OnInitDialog();	//Called right after constructor. Initialize things here.
	void  InitDialogItems();
	void SetUpFontsAndColor();
	void DrawMap( CPaintDC & dc );
	void PlatoonTracking( CPaintDC & dc );
	bool Find_MGRS( int mgrs, NextLocation location, MGRS_PIXELS &pixelCoord );
	void SetCoordinate(int nw, int north, int ne, int west, int current, int east, int sw, int south, int se, int upX, int upY);
	MGRS_COORDINATES_POSITIONS& GetCoordinate( int mgrs );
	int GetNextCoordinate( int mgrs, NextLocation location);
	void RepaintVectorMap();
	void InitilaizeMap();
};