/*
     Notes: This is a MFC template to create graphical C++ apps without
	        using the Visual Studio 2008 wizards. It is stripped down to
			a bare minimum of code so apps are as simplified as possible.
			You must disable some automated features and manually set a
			few properties for projects using this template.

			1. Create an EMPTY Win32 project. Click -> "File" -> -> "New"
			   -> "Project" -> "Win32" -> "Win32 Project".
		    2. Name is and select a directory.
			3. Select "Application Settings" -> "Empty Project" then click "Finish".
			4. Rt-click project, select "Properties" -> "Configuration Properties"
			   -> "General" -> "Use of MFC" and change it to 
			   "Use MFC in a Static Library". This will give you MFC components.
		    5. Disable Incremental Linking. Rt-click project, select "Properties" 
			   -> "Configuration Properties" -> "Linker" -> "General" ->
			   "Enable Incremental Linking" and set it to "NO".
                    6. Add a resources file. Rt-click resources and add a DIALOG object.
                    7. Change the enumerated constant to match the Dialog name.
		    8. Note that unlike 2003, when calling SetWindowText() in 2008 the
			   string passed in must be cast/converted using "L".	  

https://www.youtube.com/watch?v=Su1x9EA5Njk&index=1&list=PLinNvyf9ULFW-POkn2kU-MAQIVln0Zp8s
https://www.youtube.com/watch?v=P7d6ZrktCEo&list=PLinNvyf9ULFW-POkn2kU-MAQIVln0Zp8s&index=2

*/
//-----------------------------------------------------------------------------------------
#include "globals.h"
#include "LanNavApp.h"
#include "LandNavigation.h"

//Need a Message Map Macro for both CDialog and CWinApp

BEGIN_MESSAGE_MAP(LandNavigation, CDialog)
	ON_COMMAND( CB_START, start )
	ON_COMMAND( CB_HIDE_SHOW, hideShow )
	ON_COMMAND( CB_NORTH_WEST, dirNorthWest )
	ON_COMMAND( CB_NORTH_EAST, dirNorthEast )
	ON_COMMAND( CB_NORTH, dirNorth )
	ON_COMMAND( CB_WEST, dirWest )
	ON_COMMAND( CB_EAST, dirEast )
	ON_COMMAND( CB_SOUTH_WEST, dirSouthWest )
	ON_COMMAND( CB_SOUTH, dirSouth )
	ON_COMMAND( CB_SOUTH_EAST, dirSouthEast )
	ON_WM_TIMER()		//Installs a system timer.
    ON_WM_PAINT()		//Necessary for drawing and CImages (allows JPEGS)
	ON_WM_CTLCOLOR()	//Necessary for coloring the interface.
END_MESSAGE_MAP()

//-----------------------------------------------------------------------------------------

LandNavApp theApp;  //Starts the Application