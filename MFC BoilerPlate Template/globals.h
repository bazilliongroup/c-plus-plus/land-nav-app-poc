#pragma once

#include <afxwin.h>      //MFC core and standard components
#include "resource.h"    //main symbols
#include "XSleep.h"

#define ROW1 159
#define COL1 266
#define ROW2 171
#define COL2 255
#define ROW3 184
#define COL3 243
#define ROW4 196
#define COL4 231
#define ROW5 208
#define COL5 218
#define ROW6 220
#define COL6 206
#define ROW7 233
#define COL7 194
#define ROW8 245
#define COL8 181
#define ROW9 257
#define COL9 168
#define ROW10 271
#define COL10 156
#define ROW11 283
#define COL11 143

#define COL12 130
#define COL13 118
#define COL14 106
#define COL15 94
#define COL16 81
#define COL17 68
#define COL18 56
#define COL19 43